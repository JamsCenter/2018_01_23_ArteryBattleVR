﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WaterMarkLogo))]
public class WaterMarkLogoEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		WaterMarkLogo myWMLogo = target as WaterMarkLogo;
		EditorGUILayout.Space();
		DrawDefaultInspector();
		EditorGUILayout.Space();
		myWMLogo.normalizedLogoScale = EditorGUILayout.Slider("Horizontal normalized size of logo in screen: ",myWMLogo.normalizedLogoScale,0,1f);
		myWMLogo.logo = (Texture2D)EditorGUILayout.ObjectField("Logo: ",myWMLogo.logo,typeof(Texture2D),false);
		EditorGUILayout.Space();
		myWMLogo.tintColor = EditorGUILayout.ColorField("Tint Color: ",myWMLogo.tintColor);
		EditorGUILayout.Space();
		switch (myWMLogo.position)
		{
			case LogoPosition.LEFT_BOTTOM:
				myWMLogo.offset.x = EditorGUILayout.Slider("Left Padding (normalized): ",myWMLogo.offset.x,0,1f);
				myWMLogo.offset.y= EditorGUILayout.Slider("Bottom Padding (normalized): ",myWMLogo.offset.y,0,1f);
				break;
			case LogoPosition.LEFT_TOP:
				myWMLogo.offset.x = EditorGUILayout.Slider("Left Padding (normalized): ",myWMLogo.offset.x,0,1f);
				myWMLogo.offset.y= EditorGUILayout.Slider("Top Padding (normalized): ",myWMLogo.offset.y,0,1f);
				break;
			case LogoPosition.RIGHT_BOTTOM:
				myWMLogo.offset.x = EditorGUILayout.Slider("Right Padding (normalized): ",myWMLogo.offset.x,0,1f);
				myWMLogo.offset.y= EditorGUILayout.Slider("Bottom Padding (normalized): ",myWMLogo.offset.y,0,1f);
				break;
			case LogoPosition.RIGHT_TOP:
				myWMLogo.offset.x = EditorGUILayout.Slider("Right Padding (normalized): ",myWMLogo.offset.x,0,1f);
				myWMLogo.offset.y= EditorGUILayout.Slider("Top Padding (normalized): ",myWMLogo.offset.y,0,1f);
				break;
		}
		if (GUI.changed)
		{
			EditorUtility.SetDirty (target);
			serializedObject.ApplyModifiedProperties();
		}
	}
}