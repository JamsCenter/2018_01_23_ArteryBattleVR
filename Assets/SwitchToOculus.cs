﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class SwitchToOculus : MonoBehaviour {

    public bool m_useOculus;
    public Transform [] m_oculusCamera;
    public Transform [] m_normalCamera;

    private void Awake()
    {
        foreach (Transform item in m_oculusCamera)
        {
            item.gameObject.SetActive(XRSettings.enabled);

        }
        foreach (Transform item in m_normalCamera)
        {
            item.gameObject.SetActive(!XRSettings.enabled);

        }
    }
    
}
