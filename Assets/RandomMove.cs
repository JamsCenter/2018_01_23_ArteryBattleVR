﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RandomMove : NetworkBehaviour {

    public bool isMyCube;
    [SyncVar]
    public Vector3 position;
	
	void Update () {
      
        if (!isMyCube)
            return;
        CmdChangePosition(GetRandomVector());
        transform.Translate(Vector3.forward * Time.deltaTime);
	}
    [Command]
    public void CmdChangePosition(Vector3 pos) {
        position = pos;

        if (pos.magnitude > 50)
            RpcAutodestroy();

    }

    [ClientRpc]
    public void RpcAutodestroy()
    {
        Destroy(this.transform);
    }

    private Vector3 GetRandomVector()
    {
        throw new NotImplementedException();
    }
}
