﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ViewPlayerThrowNetwork : NetworkBehaviour {


   
    [Range(0.001f, 2f)]
    public float m_updateDelay = 0.1f;

    [Header("Head")]
    [SyncVar]
    public Vector3 m_headPosition;
    [SyncVar]
    public Quaternion m_headRotation;
    public Transform m_trackedHead;
    public Transform m_affectedHead;
    [Header("Left")]
    [SyncVar]
    public Vector3 m_leftHandPosition;
    [SyncVar]
    public Quaternion m_leftHandRotation;
    public Transform m_trackedLeftHand;
    public Transform m_affectedleftHand;

    [Header("Right")]
    [SyncVar]
    public Vector3 m_rightHandPosition;
    [SyncVar]
    public Quaternion m_rightHandRotation;
    public Transform m_trackedRightHand;
    public Transform m_affectedRightHand;

    // Use this for initialization
    IEnumerator Start () {

        while (true)
        {
            yield return new WaitForSeconds(m_updateDelay);
            if (isLocalPlayer)
            {
                CmdUpdatePositionHead(m_trackedHead.position, m_trackedHead.rotation);

            }

            
        }
	}  
	
    [Command]
    public void CmdUpdatePositionHead(Vector3 position, Quaternion rotation) {
        m_headPosition = position;
        m_headRotation = rotation;
    }

    public void Update()
    {

        m_affectedHead.position = m_headPosition;
        m_affectedHead.rotation = m_headRotation;
    }

}
