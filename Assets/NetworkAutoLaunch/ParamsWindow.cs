﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ParamsWindow : MonoBehaviour {

    public List<Params> m_paramsLoaded;
    public OnArgFound m_onArgFound;

    [Header("Debug")]
    public Text m_textDebug;

    public List<Params> m_fakeParams;

    [System.Serializable]
    public struct Params { public string m_name, m_value; }

    [System.Serializable]
    public class OnArgFound : UnityEvent<string, string> { }

    private void Start()
    {
        string[] args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            Debug.Log("ARG " + i + ": " + args[i]);
            if (args[i].StartsWith("-"))
            {
                if (m_textDebug != null)
                    m_textDebug.text += "" + "ARG " + i + ": " + args[i] + " " + args[i + 1] + "\n\r";
                m_onArgFound.Invoke(args[i], args[i + 1]);
            }
        }

#if UNITY_EDITOR
        foreach (Params p in m_fakeParams)
        {
            m_onArgFound.Invoke(p.m_name,p.m_value);

        }
#endif
    }
}
