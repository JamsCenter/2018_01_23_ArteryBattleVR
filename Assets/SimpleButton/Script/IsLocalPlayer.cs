﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class IsLocalPlayer : MonoBehaviour {

    [SerializeField]
    private bool _isLocalPlayer;
    [SerializeField]
    private bool _isServer;
    public  NetworkIdentity _networkIdentity;

    public UnityEvent _ifIsLocalPlayer;
    public UnityEvent _ifIsOtherPlayer;
    public UnityEvent _ifIsServer;

    public void Start() {

       
        if(_networkIdentity==null)
            _networkIdentity = GetComponent<NetworkIdentity>();
        _isLocalPlayer = _networkIdentity.isLocalPlayer;
        _isServer = _networkIdentity.isServer;

        if (_isLocalPlayer)
            _ifIsLocalPlayer.Invoke();
        else _ifIsOtherPlayer.Invoke();

        if (_isServer)
            _ifIsServer.Invoke();

    }

}
