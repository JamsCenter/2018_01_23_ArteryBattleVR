﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickScriptCameraRotation : MonoBehaviour
{
    [Header("Input")]
    public bool m_turnLeft;
    public bool m_turnRigh;

    [Header("Parameters")]
    public Transform m_centerAnchor;
    public float m_rotationSpeed = 90f;
    public Space m_rotaitonType;

    [Header("Debug")]
    public float m_currentHorizontal;
    public float m_previousHorizontal;

    void Update()
    {
        m_currentHorizontal = Input.GetAxis("Horizontal");
        if (m_currentHorizontal != 0f)
            if (m_currentHorizontal > 0f)
                m_turnRigh = true;
            else if (m_currentHorizontal < 0f)
                m_turnLeft = true;

        if (m_previousHorizontal != 0f && m_currentHorizontal == 0)
        {
            m_turnLeft = false;
            m_turnRigh = false;
        }

        m_previousHorizontal = m_currentHorizontal;

        if (m_turnLeft || m_turnRigh)
        {
            m_centerAnchor.Rotate(Vector3.up, m_rotationSpeed * Time.deltaTime * (m_turnRigh ? -1f : 1f), m_rotaitonType);
        }


    }

    public void SetLeftOn(bool on)
    { m_turnLeft = on; }
    public void SetRightOn(bool on)
    { m_turnRigh = on; }
}
